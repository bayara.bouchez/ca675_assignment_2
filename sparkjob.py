from pyspark.sql import SparkSession
from pyspark.sql.functions import monotonically_increasing_id
from pyspark.sql.types import StructType, StructField, IntegerType, BooleanType, ArrayType  

# First we initialize Spark
spark = SparkSession.builder.appName("Extraction_Transformation_BigQuery").getOrCreate()

# Then we load our BigQuery table as a dataframe thanks to the Google BigQuery help guides and tutorials
df = spark.read.format("bigquery").option("table", "fifth-shine-407511.12.raw").load()


# We define the JSON structure of the data written inside the "info_teams" column
json_schema = StructType([
    StructField("bans", ArrayType(StructType([
        StructField("championId", IntegerType(), True),
        StructField("pickTurn", IntegerType(), True)
    ])), True),
    StructField("objectives", StructType([
        StructField("baron", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True),
        StructField("champion", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True),
        StructField("dragon", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True),
        StructField("inhibitor", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True),
        StructField("riftHerald", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True),
        StructField("tower", StructType([
            StructField("first", BooleanType(), True),
            StructField("kills", IntegerType(), True)
        ]), True)
    ]), True),
    StructField("teamId", IntegerType(), True),
    StructField("win", BooleanType(), True)
]) 


# We transform the dataframe's column into a list using the .collect() method  
elements = df.select("info_teams").collect()

# We then load the list into a new PySpark dataframe that we will concatenate with the one it came from 
#(we tried to do it all at once but couldn't get it to work and fell back to a solution we knew would function)
 
df_info = spark.read.json(spark.sparkContext.parallelize(elements), schema=json_schema)

# To do so, we simply add a column of increasing integers (meant to represent ids) to both our dataframes, to then join them on this column.
df = df.withColumn("ids", monotonically_increasing_id())  
df_info = df_info.withColumn("ids", monotonically_increasing_id())

df_final = df.join(df_info, on="ids", how="inner")

# And we delete this unuseful row before loading the dataset back to Big Query
df_final = df_final.drop("ids")

# Finally, we setup the output table in BigQuery, that we previously manually created, to store our new data table in.
output_table = "fifth-shine-407511.12.infoTeams2"

# The process required a temporary bucket to be created (so we obliged) to go through before it could load data to BigQuery studios.
df_final.write.format("bigquery").option("temporaryGcsBucket","temporarybucket123456").option("table", output_table).mode("overwrite").save()

# We end the data processing part by shutting down the Spark session
spark.stop()


